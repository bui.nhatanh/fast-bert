import argparse
import os
from os.path import join
from xml.dom import minidom

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.utils import shuffle


def parse(file_name):
    sentences = []
    xmldoc = minidom.parse(file_name)
    review_list = xmldoc.getElementsByTagName('Review')
    for review in review_list:
        sentence_list = review.getElementsByTagName('sentence')
        for sentence in sentence_list:
            line = dict()
            line["sa_tag"] = []
            line["idx"] = sentence.attributes["id"].value.replace(":", "_")
            line["text"] = sentence.getElementsByTagName("text")[0].firstChild.wholeText
            for opinion in sentence.getElementsByTagName("Opinion"):
                category = opinion.attributes["category"].value
                polarity = opinion.attributes["polarity"].value
                sa_tag = "#".join((category, polarity))
                line["sa_tag"].append(sa_tag)
            sentences.append(line)
    return sentences


def convert_to_corpus(source_files, target_name):
    reviews = []
    for file in source_files:
        data_parsed = parse(file)
        reviews.append(data_parsed)

    mlb = MultiLabelBinarizer()
    df = pd.concat([pd.DataFrame(d) for d in reviews])
    df = df.join(pd.DataFrame(mlb.fit_transform(df.pop('sa_tag')), columns=mlb.classes_, index=df.index))
    labels = df.drop(columns=["idx", "text"]).columns.values.tolist()
    shuffle(df)
    train, val = train_test_split(df, train_size=0.8)
    train, test = train_test_split(train, train_size=0.8)

    target_folder = join(os.getcwd(), "asset", "corpus", target_name)
    if not os.path.exists(target_folder):
        os.mkdir(target_folder)
        os.mkdir(join(target_folder, "data"))
        os.mkdir(join(target_folder, "label"))
    with open(join(target_folder, "label", "labels.csv"), "w") as f:
        for label in labels:
            f.write(label + "\n")
    train_file = join(target_folder, "data", "train.csv")
    val_file = join(target_folder, "data", "val.csv")
    test_file = join(target_folder, "data", "test.csv")
    train.to_csv(train_file, index=False)
    val.to_csv(val_file, index=False)
    test.to_csv(test_file, index=False)
    print("Done!!!")


def main():
    laptop_files = [
        "asset/absa-semeval/ABSA-SemEval2015/ABSA15_Laptops_Test.xml",
        "asset/absa-semeval/ABSA-SemEval2015/ABSA-15_Laptops_Train_Data.xml",
        "asset/absa-semeval/ABSA-SemEval2015/absa-2015_laptops_trial.xml",
        "asset/absa-semeval/ABSA-SemEval2016/ABSA16_Laptops_Train_English_SB2.xml",
        "asset/absa-semeval/ABSA-SemEval2016/ABSA16_Laptops_Train_SB1_v2.xml"
    ]
    restaurant_files = [
        "asset/absa-semeval/ABSA-SemEval2015/ABSA15_Restaurants_Test.xml",
        "asset/absa-semeval/ABSA-SemEval2015/ABSA-15_Restaurants_Train_Final.xml",
        "asset/absa-semeval/ABSA-SemEval2015/absa-2015_restaurants_trial.xml",
        "asset/absa-semeval/ABSA-SemEval2016/ABSA16_Restaurants_Train_English_SB2.xml",
        "asset/absa-semeval/ABSA-SemEval2016/ABSA16_Restaurants_Train_SB1_v2.xml"
    ]

    parser = argparse.ArgumentParser(description='Pre-process for ABSA data-set')
    parser.add_argument('--domain-name', help='Domain of sentiment analysis')
    args = parser.parse_args()
    if args.domain_name == "laptops":
        convert_to_corpus(laptop_files, "laptops")

    elif args.domain_name == "restaurant":
        convert_to_corpus(restaurant_files, "restaurant")


if __name__ == '__main__':
    main()
